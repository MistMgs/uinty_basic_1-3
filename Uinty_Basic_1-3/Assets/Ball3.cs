﻿using UnityEngine;
using System.Collections;

public class Ball3 : MonoBehaviour {
    int count = 1;
    float startingPoint;
    SphereCollider myCollider;
    bool shouldPrintOver20 = true;
    bool shouldPrintOver30 = true;
	// Use this for initialization
	void Start () {
        myCollider = GetComponent<SphereCollider>();
        Debug.Log("Start");
        startingPoint = transform.position.z;

	}
	
	// Update is called once per frame
	void Update () {
        myCollider.radius = myCollider.radius + 0.01f;
        float distance;
        distance = transform.position.z- startingPoint;

       if(distance >30)
       {
           if(shouldPrintOver30)
           {Debug.Log("Over 30:" + distance);
           shouldPrintOver30 = false;
           }  
         
       }
       else if(distance >20)
       {
           if(shouldPrintOver20)
           {
               Debug.Log("Over 20:" + distance);
               shouldPrintOver20 = false;
           }
           
       }

	}   
   
}
